'use strict';

$(document).ready(function () {

  // change shop
  $('.shops').on('select2:select', function(){
    $('.change-shop .btn').removeClass('disabled');
  });

  $('.game-img').click(function(e){
    e.preventDefault();
    //if win - $('.game').addClass('niw');
    if($(this).parent().hasClass('niw')) {
      $(this).attr('data-popupGameShow','win-game');
    }
    //if lose - $('.game').addClass('esol');
    if($(this).parent().hasClass('esol')) {
      $(this).attr('data-popupGameShow','lose-game');
    }
    //if lose - $('.game').addClass('all-esol');
    if($(this).parent().hasClass('all-esol')) {
      $(this).attr('data-popupGameShow','all-lose-game');
    }
    $(this).addClass('active');
    var popupGameClass = '.' + $(this).attr('data-popupGameShow');
    $('.popup').removeClass('js-popup-show');
    $(popupGameClass).addClass('js-popup-show');
    $('body').addClass('no-scrolling');
  });

  $('.header__login.active.block-desk').click(function(e){
    e.preventDefault();
    $('.login__menu.block-desk').fadeToggle('slow');
    $('body').on('click', function (e) {
      var div = $('.header__mobNav');

      if (!div.is(e.target) && div.has(e.target).length === 0) {
        $('.login__menu.block-desk').fadeOut('slow');
      }
    });
  });

  // Mobile menu
  function openMobMenu() {
    var btn = $('#mob-menu');
    var menu = $('#header');

    btn.on('click', function () {
      $(this).toggleClass('mob-menu_active');
      menu.toggleClass('header_active');
    });
  }

  openMobMenu();

  // PopUp
  function popUp() {
    $('.js-popup-button').on('click', function (e) {
      e.preventDefault();
      var popupClass = '.' + $(this).attr('data-popupShow');
      $('.popup').removeClass('js-popup-show');
      $(popupClass).addClass('js-popup-show');
      $('body').addClass('no-scrolling');
    });

    // Close PopUp
    $('.js-close-popup').on('click', function (e) {
      e.preventDefault();
      $('.popup').removeClass('js-popup-show');
      $('body').removeClass('no-scrolling');
    });

    $('.popup').on('click', function (e) {
      var div = $('.popup__wrap');

      if (!div.is(e.target) && div.has(e.target).length === 0) {
        $('.popup').removeClass('js-popup-show');
        $('body').removeClass('no-scrolling');
      }
    });
  }
  popUp();

  // Scroll
  $('#personal-info').find('.table-wrap__inner').scrollbar();
  $('#main').find('.winners-table__wrap').scrollbar();
  $('#collections').find('.collection-list').scrollbar();
  $('.winners').scrollbar();
  // $('.popup__wrap--check').scrollbar();

  // Masked Phone
  $('input[type="tel"]').mask('+7(999)999-99-99');

  function formatState(opt) {
    if (!opt.id) {
      return opt.text;
    }

    var optimage = $(opt.element).attr('data-image');

    if (!optimage) {
      return opt.text;
    } else {
      var $opt = $(
      '<span class="select2-image"><div class="select2-image__img"><img src="' + optimage + '"/></div> ' + opt.text + '</span>');

      return $opt;
    }
  }

  $('select').each(function () {
    var placeholder = $(this).attr('data-place');

    $(this).select2({
      minimumResultsForSearch: Infinity,
      placeholder: placeholder,
      dropdownParent: $(this).closest('.form__input') });

  });

  // Select
  $('.image-select select').each(function () {
    $(this).select2({
      templateResult: formatState,
      templateSelection: formatState,
      dropdownParent: $(this).closest('.image-select') });

  });

  $('select').on('select2:open', function () {
    $('.select2-results__options').scrollbar();
  });

  // Jquery Validate
  function checkValidate() {
    var form = $('form');

    $.each(form, function () {
      $(this).validate({
        ignore: [],
        errorClass: 'error',
        validClass: 'success',
        rules: {
          code: {
            required: true
          },
          
          name: {
            required: true,
            letters: true },

          surname: {
            required: true,
            letters: true },

          secondName: {
            required: true,
            letters: true },

          passNum: {
            required: true },

          passDate: {
            required: true },

          passINN: {
            required: true },

          prizeRegion: {
            required: true },

          prizeHouse: {
            required: true },

          prizeStreet: {
            required: true },

          prizeCity: {
            required: true },

          passHead: {
            required: true },

          passAddr: {
            required: true },

          passScanINN: {
            required: true },

          kv: {
            required: true },

          email: {
            required: true,
            email: true },

          phone: {
            required: true,
            phone: true },

          city: {
            required: true,
            letters: true },

          captcha: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          password: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          home: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          apartment: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } },

          textarea: {
            required: true,
            normalizer: function normalizer(value) {
              return $.trim(value);
            } } },


        messages: {
          phone: 'Некорректный номер',
          rules: {
            required: '' },

          personal: {
            required: '' } } });


      jQuery.validator.addMethod('letters', function (value, element) {
        return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
      });

      jQuery.validator.addMethod('email', function (value, element) {
        return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
      });

      jQuery.validator.addMethod('phone', function (value, element) {
        return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
      });

    });

  }
  checkValidate();

  // Burger
  $('.header__burger').on('click', function () {
    $('.header').toggleClass('active');
  });

  $('.header__login.block-mob').on('click', function () {
    $('.header').toggleClass('active_login');
  });

  $('.back__menu').on('click', function () {
    $('.header').toggleClass('active_login');
    console.log($(this))
  });

  $('body').on('click', function (e) {
    var div = $('.header__mobNav, .header__burger, .login__menu.block-mob');

    if (!div.is(e.target) && div.has(e.target).length === 0) {
      $('.header').removeClass('active active_login');
    }
  });

  // Personal
  $('.header__personal-area').on('click', function () {
    $('.header').toggleClass('personal-active');
  });

  $('body').on('click', function (e) {
    var div = $('.header__personal-area');

    if (!div.is(e.target) && div.has(e.target).length === 0) {
      $('.header').removeClass('personal-active');
    }
  });

  // Accordion
  function openAccordion() {
    var wrap = $('#faq');
    var accordion = wrap.find('.faq__title');

    accordion.on('click', function () {
      var $this = $(this);
      var content = $this.next();

      if (content.is(':visible')) {
        $this.removeClass('active');
        content.slideUp('fast');
      } else {
        $this.addClass('active');
        content.slideDown('fast');
      }


    });
  }
  openAccordion();
});